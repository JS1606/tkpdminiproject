package main

import (
	"log"

	"github.com/tokopedia/project/nsq/messaging"
)

var topic string
var (
	messagingOptions messaging.Options
	publisherEngine  *messaging.PublisherEngine
	err              error
)

func initNsqPublisher() {
	// create messaging options
	messagingOptions = messaging.Options{
		LookupAddress:  []string{"devel-go.tkpd:4161"}, //TODO : change this with nsqd address :4161
		PublishAddress: "devel-go.tkpd:4150",           //TODO : change this with nsqd address :4150
		Prefix:         "tech_cur_nsq_",
	}
	publisherEngine, err = messaging.NewPublisherEngine(messagingOptions)
	if err != nil {
		log.Fatal(err)
	}

	topic = "hello"
}

//publish "incrVisitorCount" to increment visitor count
func nsqPublish(message string) {
	log.Println("nsq publish: " + message)

	err := publisherEngine.PublishMessage(topic, message)
	if err != nil {
		log.Fatal(err)
	}

	// // create term so the app didn't exit
	// term := make(chan os.Signal, 1)
	// signal.Notify(term, os.Interrupt, syscall.SIGTERM, syscall.SIGHUP)
	// select {
	// case <-term:
	// 	log.Println("😥 Signal terminate detected")
	// }
}
