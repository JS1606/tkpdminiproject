package main

import (
	"encoding/json"
	"fmt"
	template "html/template"
	"log"
	"net/http"
	"strconv"
)

type ErrorMessage struct {
	ErrMessage string `json:"error`
}

type OkMessage struct {
	Message string `json:"message"`
}

func initApi() {
	http.HandleFunc("/get_data", handleGetData)
	http.HandleFunc("/reset_redis", handleResetRedis)
	http.HandleFunc("/show_page", handleShowPage)
}

type HomeData struct {
	UsersHtml    template.HTML
	SearchKey    string
	VisitorCount int
}

func translateUsersToHtml(users []User) string {
	ret := ""

	for _, user := range users {
		ret +=
			fmt.Sprintf("<tr>"+
				"<td>%d</td>"+
				"<td>%s</td>"+
				"<td>%s</td>"+
				"<td>%s</td>"+
				"<td>%s</td>"+
				"<td>%s</td>"+
				"<td>%s</td>"+
				"<td>%s</td>"+
				"</tr>",
				user.UserId,
				user.FullName,
				user.Msisdn,
				user.UserEmail,
				user.BirthDate,
				user.Age,
				user.CreateTime,
				user.UpdateTime,
			)
	}
	return ret
}

func handleShowPage(w http.ResponseWriter, r *http.Request) {
	queryVal := r.URL.Query()
	filterKey := queryVal.Get("key")
	dontIncr := queryVal.Get("dontincr")

	responseData := getData(w, filterKey, dontIncr)

	homeData := HomeData{
		UsersHtml:    template.HTML(translateUsersToHtml(responseData.Users)),
		SearchKey:    filterKey,
		VisitorCount: responseData.VisitorCount,
	}

	t, err := template.ParseFiles("template/home.html") //parse the html file homepage.html
	if err != nil {                                     // if there is an error
		log.Print("template parsing error: ", err) // log it
	}
	err = t.Execute(w, homeData) //execute the template and pass it the HomePageVars struct to fill in the gaps
	if err != nil {              // if there is an error
		log.Print("template executing error: ", err) //log it
	}
}

func handleResetRedis(w http.ResponseWriter, r *http.Request) {
	err := delVisitorCount()
	if err != nil {
		writeErrorResponse(w, err)
		return
	}

	responseData := OkMessage{
		Message: "OK",
	}

	data, err := json.Marshal(responseData)
	if err != nil {
		writeErrorResponse(w, err)
		return
	}

	w.Write(data)
}

func getData(w http.ResponseWriter, filterKey, dontIncr string) ResponseData {
	if dontIncr != "1" {
		nsqPublish("incrVisitorCount")
	}

	visitorCount, err := getVisitorCount()
	if err != nil {
		writeErrorResponse(w, err)
		return ResponseData{}
	}

	visitorCountInt, err := strconv.Atoi(visitorCount)
	if err != nil {
		writeErrorResponse(w, err)
		return ResponseData{}
	}

	users, err := GetUser(filterKey)
	if err != nil {
		writeErrorResponse(w, err)
		return ResponseData{}
	}

	return ResponseData{
		Users:        users,
		VisitorCount: visitorCountInt,
	}
}

func handleGetData(w http.ResponseWriter, r *http.Request) {
	responseData := getData(w, "", "0")

	data, err := json.Marshal(responseData)
	if err != nil {
		writeErrorResponse(w, err)
		return
	}

	w.Write(data)
}

func writeErrorResponse(w http.ResponseWriter, err error) {
	if err == nil {
		return
	}

	logErrorResponse(err.Error())

	e := ErrorMessage{ErrMessage: err.Error()}
	data, _ := json.Marshal(e)
	w.Write(data)
}
