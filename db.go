package main

import (
	"database/sql"
	"fmt"
)

var postgreInfo string

func initDb() {
	postgreInfo = fmt.Sprintf("host=%v port=%v user=%v password=%v dbname=%v sslmode=disable",
		"devel-postgre.tkpd",
		"5432",
		"tkpdtraining",
		"fWstl9B7nBMP",
		"tokopedia-user")
}

func GetUser(filterKey string) ([]User, error) {
	sqlCon, err := sql.Open("postgres", postgreInfo)
	if err != nil {
		logErrorResponse("Err Init DB: " + err.Error())
		return nil, err
	}
	defer sqlCon.Close()

	query := "SELECT user_id, full_name, msisdn, user_email, birth_date, extract(year from AGE(now(), birth_date)) AS age, create_time, update_time FROM ws_user WHERE user_name is not null and update_time is not null and full_name like '%" + filterKey + "%' limit 10;"

	rows, err := sqlCon.Query(query)
	if err != nil {
		logErrorResponse("Err Query DB: " + err.Error())
		return nil, err
	}

	resUser := []User{}
	for rows.Next() {
		tempUser := User{}
		err := rows.Scan(&tempUser.UserId, &tempUser.FullName, &tempUser.Msisdn, &tempUser.UserEmail,
			&tempUser.BirthDate, &tempUser.Age, &tempUser.CreateTime, &tempUser.UpdateTime)
		if err != nil {
			logErrorResponse("Err Scan DB: " + err.Error())
			return nil, err
		}

		resUser = append(resUser, tempUser)
	}

	return resUser, nil
}
