package main

import (
	"errors"
	"log"
	"os"
	"os/signal"
	"strconv"
	"strings"
	"syscall"

	nsq "github.com/bitly/go-nsq"
	"github.com/tokopedia/project/nsq/messaging"
)

const (
	defaultConsumerMaxAttempts = 10
	defaultConsumerMaxInFlight = 100
)

func initNsqConsumer() {
	// declare var(s)
	var (
		messagingOptions messaging.Options
		consumerEngine   *messaging.ConsumerEngine
		err              error
	)

	// create messaging options
	messagingOptions = messaging.Options{
		LookupAddress:  []string{"devel-go.tkpd:4161"}, //TODO : change this with nsqd address :4161
		PublishAddress: "devel-go.tkpd:4150",           //TODO : change this with nsqd address :4150
		Prefix:         "tech_cur_nsq_",
	}
	consumerEngine = messaging.NewConsumerEngine(messagingOptions)

	// create consumer's option
	consumerOption1 := messaging.ConsumerOptions{
		Topic:       "hello",      // TODO : change this with your topic name
		Channel:     "testjosses", //TODO : change this with your channel name
		Handler:     handlerConsumer1,
		MaxAttempts: defaultConsumerMaxAttempts,
		MaxInFlight: defaultConsumerMaxInFlight,
	}

	// register consumer 1
	err = consumerEngine.RegisterConsumer(consumerOption1)
	if err != nil {
		log.Fatal(err)
	}

	// check if consumer registered
	log.Println(consumerEngine.GetConsumersNumber())

	// run the consumer engine
	consumerEngine.RunConsumer()

	// create term so the app didn't exit
	term := make(chan os.Signal, 1)
	signal.Notify(term, os.Interrupt, syscall.SIGTERM, syscall.SIGHUP)
	select {
	case <-term:
		log.Println("😥 Signal terminate detected")
	}
}

func handlerConsumer1(msg *nsq.Message) error {
	//TODO : log the message and finish it !
	messageBody := strings.Replace(string(msg.Body), "\"", "", -1)
	msg.Finish()

	log.Println("nsq consume: " + messageBody)

	if messageBody == "incrVisitorCount" {
		_, err := incrVisitor()
		if err != nil {
			return err
		}
	} else {
		return errors.New("[nsq_con][handlerConsumer1] invalid nsq message")
	}

	return nil
}

func incrVisitor() (int, error) {
	res, err := getVisitorCount()
	if err != nil {
		logErrorResponse("[project][main]" + err.Error())
		setVisitorCount(strconv.Itoa(0))
	}

	res, err = incrVisitorCount()
	if err != nil {
		logErrorResponse("[project][main]" + err.Error())
	}

	resInt, err := strconv.Atoi(res)
	if err != nil {
		logErrorResponse("[project][main]" + err.Error())
		return 0, err
	}

	return resInt, nil
}
