package messaging

import (
	"encoding/json"

	nsq "github.com/bitly/go-nsq"
)

type PublisherEngine struct {
	opt  Options
	prod *nsq.Producer
}

func NewPublisherEngine(opt Options) (*PublisherEngine, error) {
	var err error

	config := nsq.NewConfig()
	prod, err := nsq.NewProducer(opt.PublishAddress, config)
	if err != nil {
		return nil, err
	}

	return &PublisherEngine{
		opt:  opt,
		prod: prod,
	}, err
}

func (p *PublisherEngine) PublishMessage(topic string,
	data interface{}) error {
	var (
		payload []byte
		err     error
	)

	payload, err = json.Marshal(data)
	if err != nil {
		return err
	}

	topic = p.opt.Prefix + topic

	return p.prod.Publish(topic, payload)
}
