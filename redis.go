package main

import (
	"strconv"
	"time"

	redigo "github.com/garyburd/redigo/redis"
)

func newRedis(address string) *redigo.Pool {
	return &redigo.Pool{
		MaxActive:   30,
		MaxIdle:     10,
		IdleTimeout: 10 * time.Second,
		Dial: func() (redigo.Conn, error) {
			return redigo.Dial("tcp", address)
		},
	}
}

func getVisitorCount() (string, error) {
	redisPool := newRedis("127.0.0.1:6379")
	pool := redisPool.Get()

	defer redisPool.Close()

	res, err := getRedis(pool, "visitorCount")
	if err != nil {
		logErrorResponse("[redis][getVisitorCount]" + err.Error())
		return "", err
	}

	return res, nil
}

func setVisitorCount(count string) {
	redisPool := newRedis("127.0.0.1:6379")
	pool := redisPool.Get()

	defer redisPool.Close()

	err := setRedis(pool, "visitorCount", count)
	if err != nil {
		logErrorResponse("[redis][setVisitorCount]" + err.Error())
	}
}

func incrVisitorCount() (string, error) {
	redisPool := newRedis("127.0.0.1:6379")
	pool := redisPool.Get()

	defer redisPool.Close()

	res, err := incrRedis(pool, "visitorCount")
	if err != nil {
		logErrorResponse("[redis][incrVisitorCount]" + err.Error())
		return "", err
	}

	return strconv.Itoa(res), nil
}

func delVisitorCount() error {
	redisPool := newRedis("127.0.0.1:6379")
	pool := redisPool.Get()

	defer redisPool.Close()

	_, err := delRedis(pool, "visitorCount")
	if err != nil {
		logErrorResponse("[redis][delVisitorCount]" + err.Error())
		return err
	}

	return nil
}

// func execRedis(pool redigo.Conn, command string, params ...string) {
// 	log.Printf("params: %+v\n", params)
// 	resp, err := pool.Do(command, params)
// 	if err != nil {
// 		log.Println("Err Redis: ", err)
// 		return
// 	}
// 	log.Println("Resp: ", resp)
// }

func getRedis(pool redigo.Conn, key string) (string, error) {
	return redigo.String(pool.Do("GET", key))
}

func setRedis(pool redigo.Conn, key, value string) error {
	_, err := pool.Do("SET", key, value)
	return err
}

func incrRedis(pool redigo.Conn, key string) (int, error) {
	return redigo.Int(pool.Do("INCR", key))
}

func delRedis(pool redigo.Conn, key string) (int, error) {
	return redigo.Int(pool.Do("DEL", key))
}
