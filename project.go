package main

import (
	"fmt"
	"log"

	_ "github.com/lib/pq"
	grace "gopkg.in/paytm/grace.v1"
)

type User struct {
	UserId     int    `json:"user_id"`
	FullName   string `json:"full_name"`
	Msisdn     string `json:"msisdn"`
	UserEmail  string `json:"user_email"`
	BirthDate  string `json:"birth_date"`
	Age        string `json:"age"`
	CreateTime string `json:"create_time"`
	UpdateTime string `json:"update_time"`
}

type ResponseData struct {
	Users        []User `json:"users"`
	VisitorCount int    `json:"visitor_count"`
}

func main() {
	initDb()
	initApi()
	initNsqPublisher()
	go initNsqConsumer()

	fmt.Println("start serving on :8181")

	//pake grace supaya kalo diterminate, nsqnya juga keclose
	// http.ListenAndServe(":8181", nil)
	log.Fatal(grace.Serve(":8181", nil))
}
